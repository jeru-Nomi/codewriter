﻿using CodeWriterApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JerSecurityApi.Models
{
    public class Ccolumn
    {
        public string Title;

        public string ColumnType;

        public bool AutoIncrement;
        public string FKTableName;

        public string MaxCharacterLength;
        public string ColumnDataType;

        public string ObjName;

        public string ParamName;

        public Boolean IsNullable;

        public bool Checked;

        public bool IsPrimaryKey;

        public string ParentFKColumnName;
        public int RelationTypes; //יחיד ליחיד (1)או יחיד לרבים -2

        public Ccolumn()
        {

        }

        public Ccolumn GetCopy()
        {
            Ccolumn cc = new Models.Ccolumn();
            cc.Title = this.Title;

        cc.ColumnType  = this.ColumnType;

            cc.AutoIncrement = this.AutoIncrement;
            cc.FKTableName = this.FKTableName;

            cc.MaxCharacterLength = this.MaxCharacterLength;
            cc.ColumnDataType = this.ColumnDataType;

            cc.ObjName = this.ObjName;

        cc.ParamName  = this.ParamName;

            cc.IsNullable = this.IsNullable;

            cc.Checked = this.Checked;

            cc.IsPrimaryKey = this.IsPrimaryKey;

            cc.ParentFKColumnName = this.ParentFKColumnName;
            cc.RelationTypes = this.RelationTypes;
            return cc;
    }
        public object Clone()
        {
            //object t = this.MemberwiseClone();

            Ccolumn dd = (Ccolumn)this.MemberwiseClone();
            return dd;
        }
        public Ccolumn(string _ColumnName, string _ColumnType, string _ParamName, bool _IsNullable, bool _IsPrimaryKey, bool _AutoIncrement, string _MaxCheacterLength)
        {
            this.Title = _ColumnName;
            this.ColumnType = _ColumnType;
            this.ParamName = _ParamName;
            this.MaxCharacterLength = _MaxCheacterLength;
            this.IsNullable = _IsNullable;
            this.IsPrimaryKey = _IsPrimaryKey;
            this.AutoIncrement = _AutoIncrement;
            //if (this.ColumnName == null || this.ColumnName == "")
            //    this.ColumnName = _ParamName.Replace("@", "");
            this.ObjName = _ColumnName == "" ? _ParamName.Replace("@", ""): _ColumnName;
            if (this.ObjName.Contains("_"))
                this.ObjName = Converter.ObjNameFormat(this.ObjName);
            }
    }
}