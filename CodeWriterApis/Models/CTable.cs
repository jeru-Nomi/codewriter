﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JerSecurityApi.Models
{
    public class CTable: ICloneable
    {
        public string TableName;

        public bool AllCoumnsUnSelected;

        public bool SkipLeftJoin;

        public int Level;

        public string  Levelstr;

        public int tableID;

        public int ParentID;

        public List<Ccolumn> Ccolumns;

        public int RelationToParent;

        public string ColumnConnectParent;

        public CTable GetCopy()
        {
            CTable ct = new Models.CTable();
            ct.TableName = this.TableName;

            ct.AllCoumnsUnSelected = this.AllCoumnsUnSelected;

            ct.SkipLeftJoin = this.SkipLeftJoin;

            ct.Level = this.Level;

            ct.Levelstr = this.Levelstr;

            ct.tableID = this.tableID;

            ct.ParentID = this.ParentID;



            ct.RelationToParent = this.RelationToParent;

            ct.ColumnConnectParent = this.ColumnConnectParent;

            ct.Ccolumns = new List<Ccolumn>();
            for (int i = 0; i < this.Ccolumns.Count; i++)
                ct.Ccolumns.Add( this.Ccolumns[i].GetCopy());

            return ct;
        }

        public object Clone()
        {
            CTable dd =   (CTable)this.MemberwiseClone();
            for (int i = 0; i < dd.Ccolumns.Count; i++)
                dd.Ccolumns[i] = dd.Ccolumns[i].GetCopy();
            return dd;
        }
    }
}