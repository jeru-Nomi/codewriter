﻿using CodeWriterApi.Utilities;
using JerSecurityApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JerSecurityApi.ViewModels
{
    public class CrudAction
    {
        public string Action;
        public bool IsList;
        public string Procedure;
        public string ApiProcedureName;
        public string TableName;
        public List<Ccolumn> SelectColumns;
        public List<Ccolumn> Params;
        public string DalFunctionName;

        public CrudAction()
        {

        }
        public CrudAction(string _Action, bool _IsList, string _Procedure, string _TableName)
        {
            SelectColumns = new List<Models.Ccolumn>();
            Params = new List<Models.Ccolumn>();
            this.Action = _Action;
            this.IsList = _IsList;
            this.Procedure = _Procedure;
            this.TableName = _TableName;
        }
    }
}