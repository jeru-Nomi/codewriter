﻿using CodeWriterApi.Utilities;
using JerSecurityApi.Models;
using JerSecurityApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeWriterApi.ViewModels
{
    public class SqlViewModel
    {
        public string ConnectionString;

        public string ConnectionStringName;
        public string TableName;

        public Boolean DisplayRelationsColumns;

        public Boolean GetCodeValues;

        public List<CrudAction> Actions;

        public Boolean IsList;

        public string DataLayerName;

        public List<Ccolumn> uniqParams;

        public List<CTable> TablesMap;

        public List<CTable> OutputColumns;

        public List<CTable> InputColumns;

        public List<CTable> FilterColumns;

        public string sqlQuery;
        public string cSharpCode;
        public string clientSideCode;
    }
}