﻿
using CodeWriterApi.Utilities;
using CodeWriterApi.ViewModels;
using JerSecurityApi.Models;
using JerSecurityApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace CodeWriterApi.Controllers
{
    public class SqlController : BaseApiController
    {
        [HttpPost]
        [Route("api/Sql/CheckTable")]
        public IHttpActionResult CheckTable([FromBody]SqlViewModel view)
        //      public IHttpActionResult Get()
        {

            //SqlViewModel view = new ViewModels.SqlViewModel();
            //view.DisplayRelationsColumns = true;
            ////view.GetCodeValues = true;
            // view.Actions = new List<CrudAction>();
            //view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction("SELECT", true, null, null));
            //view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction("UPDATE", true, null, null));
            // view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction("INSERT", true, null, null));
            // view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction("DELETE", true, null, null));

            //view.IsList = true;
            //view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction(CrudActionName.INSERT, true, null, null));
            //view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction(CrudActionName.DELETE, true, null, null));
            //view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction(CrudActionName.UPDATE, true, null, null));
            ////view.Actions.Add(SqlActions.UPDATE);
            ////view.Actions.Add(SqlActions.DELETE);

            // view.TableName = "Sec_UserAppRoleArea";
            view.ConnectionString = @view.ConnectionString.Trim();
            view.TableName = view.TableName.Trim();

            try
            {
                DataAccess access = new DataAccess();
                view.TablesMap = new List<JerSecurityApi.Models.CTable>();

                CrudAction doSelect = view.Actions.Find(x => x.Action == CrudActionName.SELECT.ToString());

                if (doSelect != null)
                {
                    StoredProcedureBuilder.rcursiveFKCheck(view.TableName, view, 1, 0, null, "");

                    view.OutputColumns = new List<JerSecurityApi.Models.CTable>();
                    view.InputColumns = new List<JerSecurityApi.Models.CTable>();
                    view.FilterColumns = new List<JerSecurityApi.Models.CTable>();

                    ////////////filter
                    foreach (JerSecurityApi.Models.CTable tt in view.TablesMap)
                    {
                        JerSecurityApi.Models.CTable newT1 = tt.GetCopy();

                        if (view.FilterColumns.FindIndex(x => x.TableName == tt.TableName) == -1)
                        {

                            int i = 0;
                            int PKs = 0;
                            foreach (Ccolumn cc in newT1.Ccolumns)
                            {
                               // cc.Checked = true;
                                if (newT1.Level == 1)
                                {

                                    if (cc.ColumnType == "PRIMARY KEY")
                                    {
                                        //PKs++;
                                        if (!doSelect.IsList)
                                            newT1.Ccolumns[i].Checked = true;
                                    }



                                }
                                i++;
                            }
                            //if (PKs > 1)
                            //    newT.RelationToParent = 2;
                            //else
                            //    newT.RelationToParent = 1;
                            view.FilterColumns.Add(newT1);

                        }


                        if (view.InputColumns.FindIndex(x => x.TableName == tt.TableName) == -1)
                        {
                            JerSecurityApi.Models.CTable newT2 = tt.GetCopy();
                            int i = 0;
                            int PKs = 0;
                            foreach (Ccolumn cc in newT2.Ccolumns)
                            {
                                if (newT2.Level == 1)
                                {

                                    if (cc.ColumnType == "PRIMARY KEY")
                                    {
                                        PKs++;
                                        if (!doSelect.IsList)
                                            newT2.Ccolumns[i].Checked = true;
                                    }



                                }
                                i++;
                            }
                            if (PKs > 1)
                                newT2.RelationToParent = 2;
                            else
                                newT2.RelationToParent = 1;
                            view.InputColumns.Add(newT2);

                        }
                        if (view.OutputColumns.FindIndex(x => x.TableName == tt.TableName) == -1)
                        {
                            JerSecurityApi.Models.CTable newT3 = tt.GetCopy();
                            int i = 0;
                            int PKs = 0;
                            foreach (Ccolumn cc in newT3.Ccolumns)
                            {
                                if (newT3.Level == 1)
                                {
                                    newT3.Ccolumns[i].Checked = true;

                                    if (cc.ColumnType == "PRIMARY KEY")
                                    {
                                        PKs++;
                                        if (!doSelect.IsList)
                                            newT3.Ccolumns[i].Checked = false;
                                    }
                                    if (cc.Title.Contains("CreateDate") || cc.Title.Contains("CreateUserId") || cc.Title.Contains("UpdateUserId"))
                                    {
                                        newT3.Ccolumns[i].Checked = false;
                                    }

                                }
                                else
                                {

                                    if (cc.Title.ToLower().Contains("name") || cc.Title.ToLower().Contains("title") ||
                                        cc.Title.ToLower().Contains("text"))
                                    {
                                        newT3.Ccolumns[i].Checked = true;
                                    }
                                }

                                i++;
                            }

                            if (PKs > 1)
                                newT3.RelationToParent = 2;
                            else
                                newT3.RelationToParent = 1;
                            view.OutputColumns.Add(newT3);
                            //Recommended columns for select

                        }
                    }
                }

                view.OutputColumns.OrderBy(x => x.Level);
                view.InputColumns.OrderBy(x => x.Level);
                //if (view.TablesMap.Count > 1)
                //    view.DisplayRelationsColumns = true;
                DataTable columnTable = access.GetColumnName(view.TableName, new System.Data.SqlClient.SqlConnection(view.ConnectionString));

                foreach (CrudAction c in view.Actions)
                {
                    c.Procedure = Converter.formatProcName(view.TableName, c);
                }
                //string sqlQUery = StoredProcedureBuilder.BuildStoreprocedure(view.TableName,
                //                                                        columnTable,
                //                                                        new System.Data.SqlClient.SqlConnection(view.ConnectionString),
                //                                                        view.Actions);


                // gudusoft.gsqlparser.TGSqlParser sqlparser = new gudusoft.gsqlparser.TGSqlParser(gudusoft.gsqlparser.TDbVendor.DbVMssql);

                // sqlparser.SqlText.Text = sqlQUery;
                // int ret = sqlparser.PrettyPrint();
                // string s = sqlparser.ToHtml(gudusoft.gsqlparser.TOutputFmt.ofhtml);

                return Ok(new { view });

            }
            catch (Exception ex1)
            {
                //  MessageBox.Show(ex1.Message);
            }





            return Ok();

        }

        [HttpPost]
        [Route("api/Sql/GetProc")]
        public IHttpActionResult GetProc([FromBody]SqlViewModel view)
        //      public IHttpActionResult Get()
        {
            DataAccess access = new DataAccess();

            DataTable columnTable = access.GetColumnName(view.TableName, new System.Data.SqlClient.SqlConnection(view.ConnectionString));
            view.sqlQuery = StoredProcedureBuilder.BuildStoreprocedure(view
                                                                    );

            gudusoft.gsqlparser.TGSqlParser sqlparser = new gudusoft.gsqlparser.TGSqlParser(gudusoft.gsqlparser.TDbVendor.DbVMssql);

            sqlparser.SqlText.Text = view.sqlQuery;
            int ret = sqlparser.PrettyPrint();

            view.sqlQuery = sqlparser.ToProc();
            CrudAction doSelect = view.Actions.Find(x => x.Action == CrudActionName.SELECT.ToString());
            if (doSelect != null)
                view.sqlQuery = view.sqlQuery.Replace("%procselect%", doSelect.Procedure);

            CrudAction doInsert = view.Actions.Find(x => x.Action == CrudActionName.INSERT.ToString());
            if (doInsert != null)
                view.sqlQuery = view.sqlQuery.Replace("%procinsert%", doInsert.Procedure);

            CrudAction doUpdate = view.Actions.Find(x => x.Action == CrudActionName.UPDATE.ToString());
            if (doInsert != null)
                view.sqlQuery = view.sqlQuery.Replace("%procupdate%", doUpdate.Procedure);

            CrudAction doDelete = view.Actions.Find(x => x.Action == CrudActionName.DELETE.ToString());
            if (doDelete != null)
                view.sqlQuery = view.sqlQuery.Replace("%procdelete%", doDelete.Procedure);
            view.sqlQuery = view.sqlQuery.Replace("EXEC SQL", "");
            return Ok(new { view });

        }
    }
}