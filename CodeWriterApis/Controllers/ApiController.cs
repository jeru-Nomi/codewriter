﻿using CodeWriterApi.Controllers;
using CodeWriterApi.Utilities;
using CodeWriterApi.ViewModels;
using JerSecurityApi.Utilities;
using JerSecurityApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace JerSecurityApi.Controllers
{
    public class ApiController : BaseApiController
    {
        [HttpPost]
        [Route("api/api/getCode")]
        public IHttpActionResult getCode([FromBody]SqlViewModel view)
        {

            //SqlViewModel view = new SqlViewModel();
            //view.DisplayRelationsColumns = true;
            //view.GetCodeValues = true;
            //view.Actions = new List<CrudAction>();
            // view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction(CrudActionName.SELECT.ToString(), true, "SP_MTV_GET_COMMITTEE_HISTORY", null));
            //view.IsList = true;
            //view.Actions.Add(new JerSecurityApi.ViewModels.CrudAction(CrudActionName.INSERT, true, "SP_Sec_CreateUser", null));
            //view.Actions.Add(SqlActions.UPDATE);
            //view.Actions.Add(SqlActions.DELETE);

            //view.TableName = "MTV_COMITTEE_SCHEDULE_FINAL";
            //view.ConnectionString = @"Server=NTSQL-PIT12\SQLTEST16;Database=REVAHA_DEV_A;Integrated Security=true";
            //view.ConnectionStringName = "SecurityConnectionString";

            try
            {
                DataAccess access = new DataAccess();

                DataTable columnTable = access.GetColumnName(view.TableName, new System.Data.SqlClient.SqlConnection(view.ConnectionString));

                if(view.ConnectionString.Contains("Security"))
                {
                    view.ConnectionStringName = "securityConnectionString";
                }
                else
                    view.ConnectionStringName = "mitveConnectionString";

                string sqlQUery = ApiBuilder.BuildApi(view, columnTable);

                view.cSharpCode = sqlQUery;
                 //sqlQUery = ClientSideBuilder.BuildClientSide(view, columnTable);
                 //StoredProcedureBuilder.BuildStoreprocedure(view.TableName,
                 //                                                    columnTable,
                 //                                                    new System.Data.SqlClient.SqlConnection(view.ConnectionString),
                 //                                                    view.Actions);


                //     gudusoft.gsqlparser.TGSqlParser sqlparser = new gudusoft.gsqlparser.TGSqlParser(gudusoft.gsqlparser.TDbVendor.DbVMssql);

                // sqlparser.SqlText.Text = sqlQUery;
                // int ret = sqlparser.PrettyPrint();

                return Ok(view);

            }
            catch (Exception ex1)
            {
                //  MessageBox.Show(ex1.Message);
            }





            return Ok();

        }


    }
}