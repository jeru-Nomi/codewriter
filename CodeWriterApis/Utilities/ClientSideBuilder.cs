﻿using CodeWriterApi.Utilities;
using CodeWriterApi.ViewModels;
using ColorCode;
using JerSecurityApi.Models;
using JerSecurityApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace JerSecurityApi.Utilities
{
    public class ClientSideBuilder
    {
        #region declarations
        private static DataAccess _DataAccess;
        #endregion

        static ClientSideBuilder()
        {
        }
        public static string BuildClientSide(SqlViewModel aa, DataTable columnTable)
        {
            StringBuilder builder = new StringBuilder();

            GetService(aa, builder);
            // GetKendoGrid(aa, builder);
            //string actualHtml = new CodeColorizer().Colorize(builder.ToString(), Languages.JavaScript);
            //return actualHtml;
            return builder.ToString();
        }

        private static void GetService(SqlViewModel view, StringBuilder builder)
        {
            string objName = Converter.ToCamelCaseFormatTable(view.TableName);

            builder.AppendLine(string.Format("import {{ Injectable }} from '@angular/core';"));
            builder.AppendLine(string.Format("import {{ Observable }} from 'rxjs/Observable';"));
            builder.AppendLine(string.Format("import {{ BehaviorSubject }} from 'rxjs/BehaviorSubject';"));
            builder.AppendLine(string.Format("import {{ map }} from 'rxjs/operators/map';"));
            builder.AppendLine(string.Format("import 'rxjs/add/operator/do';"));
            builder.AppendLine(string.Format("import {{ Http, Response}} from '@angular/http';"));

            builder.AppendLine(string.Format("@Injectable()"));
            builder.AppendLine(string.Format("export class {0}Service extends BehaviorSubject<any[]> {{", objName));
            builder.AppendLine(string.Format("private Url: string = Global.APIMitveURL;"));
            builder.AppendLine(string.Format("constructor(private http: Http) {{;"));
            builder.AppendLine(string.Format("    super([]);"));
            builder.AppendLine(string.Format("}}"));


            foreach (CrudAction c in view.Actions)
            {
                if (c.Action == CrudActionName.SELECT.ToString())
                {
                    string Params = "";
                    string ParamsSign = "";
                    string sendParams = "";
                    Boolean viewModel = false;
                    if (c.Params.Count > 2)
                    {
                        viewModel = true;
                        sendParams = objName;
                        builder.AppendLine(string.Format("get{0}(params: any): Observable < any > {", objName));
                        builder.AppendLine(string.Format("return this.http.post(`${ this.Url}/{0}/Get/`,params)", objName));
                    }
                    else
                    {
                        foreach (Ccolumn col in c.Params)
                        {
                            ParamsSign += "{{" + col.ObjName + "}},";
                            Params += Converter.ToLowerCaseFormatColumn(col.ObjName) + ": any,";
                            sendParams += Converter.ToLowerCaseFormatColumn(col.ObjName) + '/';
                        }
                        Params = Params.TrimEnd(',');
                        ParamsSign = ParamsSign.TrimEnd(',');
                        sendParams = sendParams.TrimEnd('/');
                        builder.AppendLine(string.Format("get{0}({1}): Observable < any > {{", objName, Params));
                        if(sendParams != "")
                             builder.AppendLine(string.Format("return this.http.get(`${{ this.Url}}/{0}/Get/`+{1})", objName, sendParams));
                        else
                            builder.AppendLine(string.Format("return this.http.get(`${{ this.Url}}/{0}/Get/`)", objName, sendParams));

                    }


                    builder.AppendLine(string.Format(".pipe("));
                    builder.AppendLine(string.Format("map((response: Response) => <any>response.json()"));
                    builder.AppendLine(string.Format(")"));
                    builder.AppendLine(string.Format(").do(value => console.log(value));"));
                    builder.AppendLine(string.Format("}}"));
                }

                if (c.Action == CrudActionName.INSERT.ToString())
                {
                    builder.AppendLine(string.Format("insert{0}(params: any): Observable < any > {{", objName));
                    builder.AppendLine(string.Format("return this.http.post(`${{ this.Url}}/{0}/Post/`,params)", objName));
                    builder.AppendLine(string.Format(".pipe("));
                    builder.AppendLine(string.Format("map((response: Response) => <any>response.json()"));
                    builder.AppendLine(string.Format(")"));
                    builder.AppendLine(string.Format(").do(value => console.log(value));"));
                    builder.AppendLine(string.Format("}}"));
                }
                if (c.Action == CrudActionName.UPDATE.ToString())
                {
                    builder.AppendLine(string.Format("update{0}(params: any): Observable < any > {{", objName));


                    builder.AppendLine(string.Format("return this.http.put(`${{ this.Url}}/{0}/Put/`,params)", objName));
                    builder.AppendLine(string.Format(".pipe("));
                    builder.AppendLine(string.Format("map((response: Response) => <any>response.json()"));
                    builder.AppendLine(string.Format(")"));
                    builder.AppendLine(string.Format(").do(value => console.log(value));"));
                    builder.AppendLine(string.Format("}}"));
                }
                if (c.Action == CrudActionName.DELETE.ToString())
                {
                    string Params = "";
                    string ParamsSign = "";
                    string sendParams = "";
                    foreach (Ccolumn col in c.Params)
                    {
                        ParamsSign += "{{" + col.ObjName + "}},";
                        Params += Converter.ToLowerCaseFormatColumn(col.ObjName) + ": any,";
                        sendParams += Converter.ToLowerCaseFormatColumn(col.ObjName) + '/';
                    }
                    Params = Params.TrimEnd(',');
                    ParamsSign = ParamsSign.TrimEnd(',');
                    sendParams = sendParams.TrimEnd('/');


                    builder.AppendLine(string.Format("delete{0}(params: any): Observable < any > {{", objName));
                    builder.AppendLine(string.Format("return this.http.delete(`${{ this.Url}}/{0}/Delete/`+{1})", objName, sendParams));

                    builder.AppendLine(string.Format(".pipe("));
                    builder.AppendLine(string.Format("map((response: Response) => <any>response.json()"));
                    builder.AppendLine(string.Format(")"));
                    builder.AppendLine(string.Format(").do(value => console.log(value));"));
                    builder.AppendLine(string.Format("}}"));
                }
            }


        }


        private static void GetUtilities(SqlViewModel view, StringBuilder builder)
        {
            ///compare
            ///
            string objName = Converter.ToCamelCaseFormatTable(view.TableName);

            builder.AppendLine(string.Format("comparer{0}(otherArray) {", objName));
            builder.AppendLine(string.Format("return function(current) {", objName));
            builder.AppendLine(string.Format("return otherArray.filter(function(other) { return", objName));
            int i = 0;
            foreach (Ccolumn col in view.uniqParams)
            {
                if (i == 0)
                    builder.AppendLine(string.Format("other.{0} == current.{0}", Converter.ToLowerCaseFormatColumn(col.ObjName)));
                else
                    builder.AppendLine(string.Format("&& other.{0} == current.{0}", Converter.ToLowerCaseFormatColumn(col.ObjName)));

                i++;
                // Converter.ToLowerCaseFormatColumn
            }
            builder.AppendLine(string.Format("}).length == 0;", objName));
            builder.AppendLine(string.Format("}", objName));
            builder.AppendLine(string.Format("}", objName));
        }

        private static void GetKendoGrid(SqlViewModel view, StringBuilder builder)
        {
            string objName = Converter.ToCamelCaseFormatTable(view.TableName);


            builder.AppendLine(Helper.buildEditHandler(objName));

            builder.AppendLine(Helper.buildCancelHandler(objName));
            builder.AppendLine(Helper.buildAddHandler(objName));
            builder.AppendLine(Helper.buildSaveHandler(objName));
            builder.AppendLine(Helper.buildRemoveHandler(objName));

            return;
            //////////////////grid events and props
            builder.AppendLine(string.Format("<kendo - grid[data] = \"{0}\"",Converter.ToLowerCaseFormatColumn(objName) + "List"));
            builder.AppendLine(string.Format(" (edit) = \"edit{0}Handler($event)\"", objName));
            builder.AppendLine(string.Format(" (cancel) = \"cancel{0}Handler($event)\"", objName));
            builder.AppendLine(string.Format(" (save) = \"save{0}Handler($event)\"", objName));
            builder.AppendLine(string.Format(" (remove) = \"remove{0}Handler($event)\"", objName));
            builder.AppendLine(string.Format(" (add) = \"add{0}Handler($event)\"", objName));
            builder.AppendLine(string.Format(" (cellClick) = \"cellClick{0}Handler($event)\"", objName));
            builder.AppendLine(string.Format(" [rowClass] = \"rowCallback\""));
            builder.AppendLine(string.Format(" [filter] = \"{0}State.filter\"", Converter.ToLowerCaseFormatColumn(objName)));
            builder.AppendLine(string.Format(" [filterable] = \"true\""));
            builder.AppendLine(string.Format(" (dataStateChange) = \"{0}DataStateChange($event)\"", Converter.ToLowerCaseFormatColumn(objName)));
            builder.AppendLine(string.Format(" [sortable] = \"true\""));
            builder.AppendLine(string.Format(" [sort] = \"{0}sort\"",Converter.ToCamelCaseFormatColumn(objName) ));
            builder.AppendLine(string.Format(" (sortChange) = \"{0}SortChange($event)\"", Converter.ToLowerCaseFormatColumn(objName)));

            ////////////////////grid header
            builder.AppendLine(string.Format("<ng-template kendoGridToolbarTemplate>"));
            builder.AppendLine(string.Format("<button kendoGridAddCommand type=\"button\" >Add</button>"));
            builder.AppendLine(string.Format("</ng-template>"));

            
            /////////////////grid columns
            foreach(Ccolumn col in view.uniqParams)
            {
                builder.AppendLine(string.Format("<kendo-grid-column field=\"{0}\" title =\"{0}\" > ",Converter.ToLowerCaseFormatColumn(col.Title)));

                if (col.ColumnType == "int")
                {
                    builder.AppendLine(Helper.GetKendoGridFilterDropDownTemplate(Converter.ToLowerCaseFormatColumn(col.ObjName)));
                    builder.AppendLine(Helper.GetKendoGridCellTemplate(Converter.ToLowerCaseFormatColumn(col.ObjName)));
                    builder.AppendLine(Helper.GetKendoGridDropDownEditTemplate(Converter.ToLowerCaseFormatColumn(col.ObjName)));
                    ////filter column

                }
                if(col.ColumnType == "string")
                    builder.AppendLine(Helper.GetKendoGridTextEditTemplate(Converter.ToLowerCaseFormatColumn(col.ObjName)));
                
                if(col.ColumnType == "datetime")
                {

                }



                //builder.AppendLine(string.Format(@"< kendo - grid - command - column title ='uu' width = '220' >
                //                     < ng - template kendoGridCellTemplate let-isNew = 'isNew' let - dataItem ='dataItem' >
                //                              < button kendoGridRemoveCommand type = 'button' * ngIf = 'dataItem.insertModuleID == 2' > הסר </ button >
                //                               < button kendoGridSaveCommand* ngIf = 'isNew'  type = 'button'[disabled] = 'myForm.invalid || myForm.pristine' >{
                //                                {{
                //                                isNew ? 'הוסף' : 'עדכן' }
                //                                 }}</ button >

                //                            < button kendoGridCancelCommand type = 'button' >{ { isNew ? ' ביטול' : 'הסר' } }</ button >

                //                    </ ng - template >
                //                        </ kendo - grid - command - column >

                //                    </ kendo - grid >"));


                //build functions
                //builder.AppendLine(Helper.buildKendoGridParams(col.ObjName));


             



                //    builder.AppendLine(Helper.buildFilterFunction(col.ObjName));











                //< ng - template kendoGridFilterCellTemplate let-filter = "filter" >

                //                         < kendo - dropdownlist
                //                         [textField] = "'title'"
                //                         [data] = "helpTypes"
                //                         [popupSettings] = "{ width: 'auto'}"
                //                         [valueField] = "'id'"
                //                         [value] = "filter"
                //                         (valueChange) = "currentFilterChange($event, filterService, 'serviceID')"
                //                         >



            }

        }
    }
}