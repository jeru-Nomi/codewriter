﻿using CodeWriterApi.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace JerSecurityApi.Utilities
{
    public static class Helper
    {
        public static string GetKendoGridFilterDropDownTemplate(string ColumnName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("<ng - template kendoGridFilterCellTemplate let-filter = \"filter\" >"));
            builder.Append(string.Format("<kendo-dropdownlist"));
            builder.Append(string.Format("[textField] = \"title\""));
            builder.Append(string.Format("[data] = \"{0}\"", ColumnName + "List"));
            builder.Append(string.Format("[popupSettings] = \"{{ width: 'auto'}}\""));
            builder.Append(string.Format("[valueField] = \"id\""));
            builder.Append(string.Format("[value] = \"filter\""));
            builder.Append(string.Format("(valueChange) = \"filter{0}($event, filterService, '{0}')\"", ColumnName));
            builder.Append(string.Format(">"));
            builder.Append(string.Format("</ kendo - dropdownlist >"));
            builder.Append(string.Format("</ng-template> "));

            return builder.ToString();
        }



        public static string GetKendoGridCellTemplate(string ColumnName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("<ng-template kendoGridCellTemplate let-dataItem=\"dataItem\" > "));
            builder.Append(string.Format("{{dataItem.{0}}}", ColumnName));
            builder.Append(string.Format("</ng-template> "));

            return builder.ToString();
        }


        public static string GetKendoGridDropDownEditTemplate(string ColumnName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("<ng - template kendoGridEditTemplate let-dataItem = \"dataItem\" let-rowIndex = \"rowIndex\" >"));
            builder.Append(string.Format("<kendo-dropdownlist"));
            builder.Append(string.Format("[name] = \"{0}{{rowIndex}}\"", ColumnName));
            builder.Append(string.Format("[textField] = \"title\""));
            builder.Append(string.Format("[data] = \"{0}\"", ColumnName + "List"));
            builder.Append(string.Format("[popupSettings] = \"{{ width: 'auto'}}\""));
            builder.Append(string.Format("[valuePrimitive] = \"true\""));
            builder.Append(string.Format("[valueField] = \"id\""));
            builder.Append(string.Format("[value] = \"filter\""));
            builder.Append(string.Format("[(ngModel)] = \"dataItem.{0}\"", ColumnName));
            builder.Append(string.Format(">"));
            builder.Append(string.Format("</ kendo - dropdownlist >"));
            builder.Append(string.Format("</ng-template> "));

            return builder.ToString();
        }

        public static string GetKendoGridTextEditTemplate(string ColumnName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("<ng - template kendoGridEditTemplate let-dataItem = \"dataItem\" >"));
            builder.Append(string.Format(" < input[(ngModel)] = \"dataItem.{0}\" kendoGridFocusable name = \"frameName{0}\" class=\"k - textbox\" />", ColumnName));
            builder.Append(string.Format("</ng-template> "));

            return builder.ToString();
        }

        public static string buildKendoGridParams(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("{0}List: any = [];", Converter.ToLowerCaseFormatColumn(objName)));
            builder.Append(string.Format("orginal{0}List: any = [];", objName));
            builder.Append(string.Format("orginal{0}List: any = [];", objName));

            builder.Append(string.Format(@"public filter: CompositeFilterDescriptor;
                                         public {0}sort: SortDescriptor[] = [];
                                         public {0}state: State = {
                                        filter: {
                                            logic: 'and',
                                            filters: [
                                                    ]
                                                }
                                                };", Converter.ToLowerCaseFormatColumn(objName)));

            builder.Append(string.Format("private edited{0}RowIndex: number;", objName));

            return builder.ToString();
        }
        public static string buildEditHandler(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("public edit{0}Handler({{ sender, rowIndex, dataItem }}) {{", objName));
            builder.Append(string.Format("this.close{0}Editor(sender);", objName));
            builder.Append(string.Format("this.edited{0} = Object.assign({{}}, dataItem);", objName));
            builder.Append(string.Format("sender.editRow(rowIndex)", objName));
            builder.Append(string.Format("}}"));
            return builder.ToString();
        }


        public static string buildCancelHandler(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("public cancel{0}Handler({{ sender, rowIndex }}) {{", objName));
            builder.Append(string.Format("this.close{0}Editor(sender);", objName));
            builder.Append(string.Format("}}"));
            return builder.ToString();
        }

        public static string buildAddHandler(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("public add{0}Handler({{ sender, rowIndex }}) {{", objName));
            builder.Append(string.Format("this.close{0}Editor(sender);", objName));
            builder.Append(string.Format("sender.addRow(new {0}Row());", objName));
            builder.Append(string.Format("}}"));
            return builder.ToString();
        }

        public static string buildSaveHandler(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("public save{0}Handler({{ sender, rowIndex, dataItem, isNew }}) {{", objName));
            builder.Append(string.Format("if (isNew){{"));
            builder.Append(string.Format("this.{0}Service.insert{1}(service).subscribe(data => {{}});}}",Converter.ToLowerCaseFormatColumn( objName),  objName));

            builder.Append(string.Format("sender.closeRow(rowIndex);"));
            builder.Append(string.Format("this.edited{0}RowIndex = undefined", objName));
            
            builder.Append(string.Format("}}"));
            return builder.ToString();
        }

        public static string buildRemoveHandler(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("public remove{0}Handler({{ dataItem }}) {{", objName));
            builder.Append(string.Format("this.{0}Service.delete{1}(service).subscribe(data => {{}});}}", Converter.ToLowerCaseFormatColumn(objName), objName));
            builder.Append(string.Format("}}"));
            return builder.ToString();
        }

        public static string buildFilterFunction(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("public filter{0}(values: any[], filterService: FilterService, filter: any): void {{", objName));
            builder.Append(string.Format("this.utilitiesService.addFilterToState(values, filter, this.state)"));
            builder.Append(string.Format("this.{0}List = process(_.clone(this.orginal{0}List), this.state);", Converter.ToLowerCaseFormatColumn(objName)));
            builder.Append(string.Format("}}"));
            return builder.ToString();
        }

        public static string buildProcSignuture(string FunnyAuthor, string TableName)
        {

            String[] camelCaseWords = SplitCamelCase(TableName);
            StringBuilder builder = new StringBuilder();
            builder.Remove(0, builder.Length);
            builder.AppendLine("-- =============================================");
            builder.Append(string.Format("--Author:		{0}", FunnyAuthor));
            builder.AppendLine(String.Format("--Create date: {0}", DateTime.Now.ToShortDateString()));
            builder.AppendLine(String.Format("-- Description: {0}", ""));
            builder.AppendLine("-- =============================================");
            return builder.ToString();
        }



//public static class ObjectExtensions
//        {
            public static T Clone<T>(this T obj)
            {
                return (T)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(obj));
            }
        //}

        public static T DeepCopy<T>(T other)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Context = new StreamingContext(StreamingContextStates.Clone);
                formatter.Serialize(ms, other);
                ms.Position = 0;
                return (T)formatter.Deserialize(ms);
            }
        }

        public static string getCreateDateColumn(string title)
        {
            title = "@" + title;
            if (title == "@CreateDate")
            {
                title = "GetDate()";
            }
            if (title == "@" + "CreateUserId")
            {
                title = "dbo.ad_session_user_id()";
            }
            return title;

        }

        static string[]  SplitCamelCase(string source)
        {
            return Regex.Split(source, @"(?<!^)(?=[A-Z])(_)");
        }



        public static string BuildStateChange(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format(@"public {0}sortChange(sort: SortDescriptor[]): void {
                                            this.{0}sort = sort;
                                            this.{0}List = orderBy(this.{0}List, this.{0}sort)
                                           }

                                        {0}DataStateChange(state: DataStateChangeEvent): void {
                                        this.{0}state = state;
                                        this.{0}List = process(Object.assign({ }, this.orginal{0}List), this.{0}state);
                                            }", Converter.ToLowerCaseFormatColumn( objName)));
            return builder.ToString();
        }

        public static string BuildGetKendoData(string objName)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format(@" this.{0}Service.get{0}List()
                                                 .subscribe(data => {
                                                    this.{0}List = data.{0}List;
                                                    this.orginal{1}List = this.{0}List.map(a => ({...a
                                                     }));});", Converter.ToLowerCaseFormatColumn(objName), objName));
            return builder.ToString();
        }

       
    }
}