﻿/******************************************************************
 * Author: Saket Sinha
 * Date Created: 24 June 2007
 * Please do not remove or change the Authors name if using this
 * utility
 * Copyright: Saket Sinha Australia
 *****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using JerSecurityApi.ViewModels;
using JerSecurityApi.Models;
using System.Threading;
using System.Threading.Tasks;
using JerSecurityApi.Utilities;
using CodeWriterApi.ViewModels;

namespace CodeWriterApi.Utilities
{
    public class StoredProcedureBuilder
    {
        #region declarations
        private static DataAccess _DataAccess;
        #endregion

        #region Constructor
        static StoredProcedureBuilder()
        {
        }
        public static string BuildStoreprocedure(SqlViewModel view)
        {
            string result = "";
            StringBuilder builder = new StringBuilder();
            CrudAction doSelect = view.Actions.Find(x => x.Action == CrudActionName.SELECT.ToString());
            if (doSelect != null)
            {
                try
                {

                    result += builder.AppendLine(Helper.buildProcSignuture("Nomi Gantz", view.TableName));
                    result += builder.AppendLine(string.Format("CREATE PROCEDURE [DBO].[{0}]", "%procselect%"));
                    result += BuildSelectProcedure(view, doSelect);
                }
                catch (Exception ex)
                {
                    //  MessageBox.Show(ex.Message);
                }
            }
            builder = new StringBuilder();
            CrudAction doInsert = view.Actions.Find(x => x.Action == CrudActionName.INSERT.ToString());
            if (doInsert != null)
            {
                try
                {
                    result += builder.Remove(0, builder.Length);
                    result += builder.AppendLine(string.Format("CREATE PROCEDURE [DBO].[{0}]", "%procinsert%"));
                    result += BuildInsertProcedure(view.InputColumns[0], builder);
                }
                catch (Exception ex1)
                {
                    // MessageBox.Show(ex1.Message);
                }
            }
            builder = new StringBuilder();

            CrudAction doDelete = view.Actions.Find(x => x.Action == CrudActionName.DELETE.ToString());

            if (doDelete != null)
            {
                try
                {
                    result += builder.Remove(0, builder.Length);
                    result += builder.AppendLine(string.Format("CREATE PROCEDURE [DBO].[{0}]", "%procdelete%"));
                    result += BuildDeleteProcedure(view.InputColumns[0], builder);
                }
                catch (Exception ex1)
                {
                    //  MessageBox.Show(ex1.Message);
                }
            }
            CrudAction doUpdate = view.Actions.Find(x => x.Action == CrudActionName.UPDATE.ToString());

            if (doUpdate != null)
            {
                try
                {
                    result += builder.Remove(0, builder.Length);
                    result += builder.AppendLine(string.Format("CREATE PROCEDURE [DBO].[{0}]", "%procupdate%"));
                    result += BuildUpdateProcedure(view.InputColumns[0], builder);
                }
                catch (Exception ex1)
                {
                    //MessageBox.Show(ex1.Message);
                }
            }
            return result;
        }
        #endregion

        public static int _count = -1;

        public static void rcursiveFKCheck(string tableName, ViewModels.SqlViewModel view, int level, int parentID, string ParentFKColumnName, string FullLevelMap)
        {
            Interlocked.Increment(ref _count);
            //List<Task> tasks = new List<Task>();
            try
            {
                DataAccess access = new DataAccess();
                DataTable columnTable = access.GetColumnName(tableName, new System.Data.SqlClient.SqlConnection(view.ConnectionString));
                CTable ct = new JerSecurityApi.Models.CTable();
                FullLevelMap += parentID + 1.ToString() + ",";
                ct.TableName = tableName;
                ct.Level = level;
                ct.Levelstr += FullLevelMap;
                ct.tableID = view.TablesMap.Count + 1;
                ct.Ccolumns = new List<Ccolumn>();
                ct.ParentID = parentID;
                List<string> FKs = new List<string>();
                List<string> FKsc = new List<string>();
                string ParentFKColumnNameBack = ParentFKColumnName;
                foreach (DataRow row in columnTable.Rows)
                {
                    Ccolumn cc = new JerSecurityApi.Models.Ccolumn();
                    cc.Title = row["COLUMN_NAME"].ToString();
                    cc.IsNullable = Convert.ToBoolean(row["is_nullable"]);
                    cc.ColumnDataType = row["DATA_TYPE"].ToString();
                    cc.ColumnType = row["table_type"].ToString();
                    cc.FKTableName = row["table_name"].ToString();
                    cc.AutoIncrement = Convert.ToBoolean(row["AUTO_INCREMENT"]);
                    cc.ObjName = getObjName(row["COLUMN_NAME"].ToString());
                    if (row["table_name"].ToString() != null && row["table_name"].ToString() != "")
                    {
                        ParentFKColumnName = row["COLUMN_NAME"].ToString();
                        FKs.Add(row["table_name"].ToString());
                        FKsc.Add(row["COLUMN_NAME"].ToString());
                    }
                    if (row["COLUMN_NAME"].ToString() == ParentFKColumnName)
                    {
                        cc.ParentFKColumnName = ParentFKColumnName;
                    }
                    cc.IsPrimaryKey = Convert.ToBoolean(row["Primary Key"]);
                    //if (cc.Title == ParentFKColumnNameBack)
                    //    cc.ParentFKColumnName = ParentFKColumnNameBack;


                    ct.Ccolumns.Add(cc);
                }
                view.TablesMap.Add(ct);

                level++;
                if (level == 4)
                    return;

                //if (level == 4 || FKs.Count == 0)
                int i = 0;
                foreach (string subTblName in FKs)
                {

                    rcursiveFKCheck(subTblName, view, level, ct.tableID, FKsc[i], FullLevelMap);
                    i++;
                    //Task task = Task.Factory.StartNew(() => );
                    //tasks.Add(task);

                    //    foreach (string f in Directory.GetFiles(d))
                    //    {
                    //        Console.WriteLine(f);
                }
                //    DirSearch(d);
                //}
            }
            catch (System.Exception excpt)
            {

                Console.WriteLine(excpt.Message);
            }
            //return Task.WhenAll(tasks);
        }


        #region Private Methods
        private static string getObjName(string _ColumnName)
        {

            string ObjName = _ColumnName;
            if (ObjName.Contains("_"))
                ObjName = Converter.ObjNameFormat(ObjName);


            return ObjName;
        }

        private static string BuildUpdateProcedure(CTable ct, StringBuilder insertBuilder)
        {
            //try
            //{
            //updateBuilder.AppendLine(string.Format("[{0}Update] (", tableName));

            //foreach (DataRow row in columnTable.Rows)
            //{
            //    if ((row["Data_Type"].ToString().Equals("varchar")))
            //    {
            //        updateBuilder.Append(string.Format("@{0} AS {1}({2}) ,", row["Column_Name"], row["Data_Type"].ToString().ToUpper(), row["Character_Maximum_Length"]));
            //    }
            //    else
            //    {
            //        updateBuilder.Append(string.Format("@{0} AS {1} ,", row["Column_Name"], row["Data_Type"].ToString().ToUpper()));
            //    }
            //}
            //updateBuilder = RemoveLastComma(updateBuilder);
            //updateBuilder.Append(") AS ");

            //updateBuilder.AppendLine(string.Format("UPDATE {0} SET ", tableName));
            //foreach (DataRow row in columnTable.Rows)
            //{
            //    if (!row.Equals(columnTable.Rows[0]))
            //    {
            //        updateBuilder.Append(string.Format("[{0}] = @{0},", row["Column_Name"]));
            //    }
            //}
            //updateBuilder = RemoveLastComma(updateBuilder);
            //updateBuilder.AppendLine(string.Format(" WHERE [{0}] = @{0}", columnTable.Rows[0]["Column_Name"]));
            //DataAccess.CreateProcedure(updateBuilder.ToString(), connection);
            //   AuditLog.AppendLog(string.Format("Update stored procedure created for {0} at {1}:{2}", tableName, DateTime.Today.ToLongDateString(), DateTime.Now));
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}


            insertBuilder = new StringBuilder();
            try
            {

                insertBuilder.AppendLine(string.Format("("));
                int k = 1;
                foreach (Ccolumn c in ct.Ccolumns)
                {
                    if (c.Title != "CreateDate" && c.Title != "CreateUserId")
                    {
                        string nullableStr = "";
                        if (c.IsNullable)
                            nullableStr = " = null";
                        if (c.ColumnDataType.ToString().Equals("varchar"))
                        {
                            insertBuilder.AppendLine(string.Format("@{0} {1}({2}){3},", c.Title, c.ColumnDataType.ToUpper(), c.MaxCharacterLength, nullableStr));
                        }
                        else
                        {
                            insertBuilder.AppendLine(string.Format("@{0} {1}{2},", c.Title, c.ColumnDataType.ToUpper(), nullableStr));
                        }
                    }

                    k++;
                }
                insertBuilder = RemoveLastComma2(insertBuilder);
                insertBuilder.AppendLine(string.Format(")"));
                insertBuilder = RemoveLastComma(insertBuilder);
                insertBuilder.AppendLine("AS");

                insertBuilder.AppendLine(string.Format("UPDATE {0} SET ", ct.TableName));
                //int i = 1;
                StringBuilder wheres = new StringBuilder();
                if (ct.Ccolumns.FindAll(x => x.IsPrimaryKey).Count > 0)
                    wheres.Append(string.Format("WHERE 1=1 "));
                foreach (Ccolumn c in ct.Ccolumns)
                {
                    if (!c.IsPrimaryKey)
                        //if (!row.Equals(columnTable.Rows[0]))
                        //{
                        //if (i < ct.Ccolumns.Count)
                        insertBuilder.AppendLine(string.Format("{0} = {1},", c.Title, Helper.getCreateDateColumn(c.Title)));
                    else
                        wheres.AppendLine(string.Format("and {0} = {1}", c.Title, Helper.getCreateDateColumn(c.Title)));
                    //app
                    //    insertBuilder.AppendLine(string.Format("{0}", c.Title));
                    //i++;
                    //}
                }
                insertBuilder = RemoveLastComma2(insertBuilder);
                if (ct.Ccolumns.FindAll(x => x.IsPrimaryKey).Count > 0)
                    insertBuilder.Append(wheres);
                ////foreach (Ccolumn c in ct.Ccolumns)
                ////{

                ////    if (!c.AutoIncrement)
                ////    {
                ////        string title = "@" + c.Title;
                ////        if (title == "@CreateDate")
                ////        {
                ////            title = "GetDate()";
                ////        }
                ////        if (title == "@" + "CreateUserId")
                ////        {
                ////            title = "dbo.ad_session_user_id()";
                ////        }
                ////        insertBuilder.AppendLine(string.Format("{0},", title));
                ////    }

                ////}
                //insertBuilder = RemoveLastComma(insertBuilder);
                insertBuilder = insertBuilder.Remove(insertBuilder.Length - 1, 1);
                insertBuilder = insertBuilder.Remove(insertBuilder.Length - 1, 1);


                //  DataAccess.CreateProcedure(insertBuilder.ToString(), connection);
                //AuditLog.AppendLog(string.Format("Insert stored procedure created for {0} at {1}:{2}", tableName, DateTime.Today.ToLongDateString(), DateTime.Now));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return insertBuilder.ToString();
        }

        private static string BuildDeleteProcedure(CTable ct, StringBuilder insertBuilder)
        {
            insertBuilder = new StringBuilder();
            try
            {
                foreach (Ccolumn c in ct.Ccolumns)
                {
                    if (c.IsPrimaryKey)
                    {
                        insertBuilder.AppendLine(string.Format("@{0} {1},", c.Title, c.ColumnDataType.ToUpper()));
                    }

                }
                insertBuilder = RemoveLastComma2(insertBuilder);
                insertBuilder.AppendLine("AS");
                insertBuilder.AppendLine(string.Format("DELETE {0} ", ct.TableName));

                insertBuilder.Append(string.Format("WHERE 1=1 "));
                //int i = 1;
                foreach (Ccolumn c in ct.Ccolumns)
                {
                    if (c.IsPrimaryKey)
                        //if (!row.Equals(columnTable.Rows[0]))
                        //{
                        //if (i < ct.Ccolumns.Count)
                        insertBuilder.AppendLine(string.Format("and {0} = @{0},", c.Title));
                    //app
                    //    insertBuilder.AppendLine(string.Format("{0}", c.Title));
                    //i++;
                    //}
                }
                insertBuilder = RemoveLastComma2(insertBuilder);
                return insertBuilder.ToString();
                // DataAccess.CreateProcedure(deleteBuilder.ToString(), connection);
                ///AuditLog.AppendLog(string.Format("Delete stored procedure created for {0} at {1}:{2}", tableName, DateTime.Today.ToLongDateString(), DateTime.Now));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string BuildSelectProcedure(SqlViewModel view, CrudAction cc)
        {
            try
            {
                StringBuilder selectBuilder = new StringBuilder();
                StringBuilder whereBuilder = new StringBuilder();

                //if (cc.IsList)
                //{
                whereBuilder.AppendLine(string.Format("where 1=1 "));
                bool whereExist = false;
                foreach (CTable ct in view.FilterColumns)
                {
                    foreach (Ccolumn c in ct.Ccolumns)
                    {
                        //if (c.IsPrimaryKey)
                        //  whereBuilder.AppendLine(string.Format("( @{0} AS INT = NULL) AS ", "SOME_FILTER"));
                        if (c.Checked)
                        {
                            selectBuilder.AppendLine(string.Format(" @{0} AS {1}, AS ", c.Title, c.ColumnDataType));
                            whereBuilder.AppendLine(string.Format("and t{1}.{0} = @{0} ", c.Title, ct.tableID.ToString()));
                            whereExist = true;
                        }
                        // selectBuilder.AppendLine(string.Format(" @{0} AS INT, AS ", c.ObjName));
                    }
                }
                if(whereExist)
                selectBuilder = RemoveLastComma2(selectBuilder);

                //}
                //else
                //{
                //    foreach (Ccolumn c in view.OutputColumns[0].Ccolumns)
                //    {
                //        if (c.IsPrimaryKey)
                //            selectBuilder.AppendLine(string.Format(" @{0} AS INT, AS ", c.ObjName));
                //    }
                //}
                //selectBuilder = RemoveLastComma(selectBuilder);
                selectBuilder.AppendLine("SELECT");
                StringBuilder selectColumnBuilder = new StringBuilder();

                StringBuilder leftJoinBuilder = new StringBuilder();
                leftJoinBuilder.AppendLine();
                leftJoinBuilder.AppendLine(string.Format("from {0} t1", view.OutputColumns[0].TableName));
                int i = 1;
                foreach (CTable ct in view.OutputColumns)
                {
                    int countSelected = ct.Ccolumns.FindAll(x => x.Checked).Count;
                    if (countSelected == 0)
                        ct.AllCoumnsUnSelected = true;

                }

                foreach (CTable ct in view.OutputColumns)
                {
                    if (ct.AllCoumnsUnSelected)
                    {
                        int countChildrenSelected = view.OutputColumns.FindAll(x => !x.AllCoumnsUnSelected && x.Levelstr.Contains(ct.tableID.ToString())).Count;
                        if (countChildrenSelected == 0)
                            ct.SkipLeftJoin = true;
                    }
                }


                foreach (CTable ct in view.OutputColumns.FindAll(x => !x.SkipLeftJoin))
                {
                    List<string> columnFK = new List<string>();
                    foreach (Ccolumn ccc in ct.Ccolumns)
                    {
                        if (ccc.ParentFKColumnName == ccc.Title && ccc.FKTableName == "")
                            columnFK.Add(ccc.Title);
                        if (ccc.Checked)
                        {
                            selectColumnBuilder.AppendLine(string.Format("t{0}.{1},", ct.tableID.ToString(), ccc.Title));
                        }
                    }

                    if (i > 1)
                    {
                        StringBuilder bb = new StringBuilder();
                        int v = 1;
                        foreach (string s in columnFK)
                        {
                            if (v == 1)
                                bb.AppendLine(string.Format(" on t{0}.{1} = t{2}.{1}", ct.tableID.ToString(), s, ct.ParentID));
                            else
                                bb.AppendLine(string.Format(" and t{0}.{1} = t{2}.{1}", ct.tableID.ToString(), s, ct.ParentID));

                            v++;
                        }
                        leftJoinBuilder.AppendLine(string.Format("left join {0} t{1} {2} ", ct.TableName, ct.tableID.ToString(), bb.ToString()));

                    }
                    i++;

                }

                // selectColumnBuilder = RemoveLastComma(selectColumnBuilder);
                selectColumnBuilder.Remove(selectColumnBuilder.Length - 1, 1);
                selectColumnBuilder.Remove(selectColumnBuilder.Length - 1, 1);


                selectBuilder.Append(selectColumnBuilder);
                selectColumnBuilder.AppendLine();
                selectBuilder = RemoveLastComma(selectBuilder);
                selectBuilder.Append(leftJoinBuilder);

                if (whereExist)
                    selectBuilder.Append(whereBuilder);
                //foreach (DataRow row in columnTable.Rows)
                //{
                //    selectBuilder.Append(string.Format("{0},", row["Column_Name"]));
                //}
                //selectBuilder = RemoveLastComma(selectBuilder);
                //selectBuilder.AppendLine(string.Format(" FROM {0} ", tableName));
                //if (!IsList)
                //{
                //    int I = 0;
                //    foreach (DataRow row in columnTable.Rows)
                //    {

                //        if (Convert.ToBoolean(row["Primary Key"]))
                //        {
                //            if (I == 0)
                //            {
                //                selectBuilder.AppendLine(string.Format("WHERE {0} = @{0}", row["Column_Name"]));

                //            }
                //            else
                //            {
                //                selectBuilder.AppendLine(string.Format("AND {0} = @{0}", row["Column_Name"]));
                //            }
                //            I++;
                //        }

                //    }
                //}
                return selectBuilder.ToString();

                //DataAccess.CreateProcedure(selectBuilder.ToString(), connection);
                //AuditLog.AppendLog(string.Format("Select stored procedure created for {0} at {1}:{2}", tableName, DateTime.Today.ToLongDateString(), DateTime.Now));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        private static string BuildInsertProcedure(CTable ct, StringBuilder insertBuilder)
        {
            insertBuilder = new StringBuilder();
            try
            {

                insertBuilder.AppendLine(string.Format("("));
                int k = 1;
                foreach (Ccolumn c in ct.Ccolumns)
                {
                    if (!c.AutoIncrement && c.Title != "CreateDate" && c.Title != "CreateUserId")
                    {
                        string nullableStr = "";
                        if (c.IsNullable)
                            nullableStr = " = null";
                        if (c.ColumnDataType.ToString().Equals("varchar"))
                        {

                            insertBuilder.AppendLine(string.Format("@{0} {1}({2}){3},", c.Title, c.ColumnDataType.ToUpper(), c.MaxCharacterLength, nullableStr));


                        }
                        else
                        {
                            insertBuilder.AppendLine(string.Format("@{0} {1}{2},", c.Title, c.ColumnDataType.ToUpper(), nullableStr));

                        }
                    }

                    k++;
                }
                insertBuilder = RemoveLastComma2(insertBuilder);
                insertBuilder.AppendLine(string.Format(")"));
                insertBuilder = RemoveLastComma(insertBuilder);
                insertBuilder.AppendLine("AS");

                insertBuilder.AppendLine(string.Format("INSERT INTO {0} ( ", ct.TableName));
                int i = 1;
                foreach (Ccolumn c in ct.Ccolumns)
                {
                    //if (!row.Equals(columnTable.Rows[0]))
                    //{
                    if (i < ct.Ccolumns.Count)
                        insertBuilder.AppendLine(string.Format("{0},", c.Title));
                    else
                        insertBuilder.AppendLine(string.Format("{0}", c.Title));
                    i++;
                    //}
                }
                insertBuilder = insertBuilder.Remove(insertBuilder.Length - 1, 1);
                insertBuilder = insertBuilder.Remove(insertBuilder.Length - 1, 1);
                insertBuilder.AppendLine(")");
                insertBuilder.AppendLine("VALUES");
                insertBuilder.Append("(");
                int j = 1;
                foreach (Ccolumn c in ct.Ccolumns)
                {

                    if (!c.AutoIncrement)
                    {
                        string title = Helper.getCreateDateColumn(c.Title);
                        if (j < ct.Ccolumns.Count)
                            insertBuilder.AppendLine(string.Format("{0},", title));
                        else
                            insertBuilder.AppendLine(string.Format("{0}", title));
                    }
                    j++;
                }
                //insertBuilder = RemoveLastComma(insertBuilder);
                insertBuilder = insertBuilder.Remove(insertBuilder.Length - 1, 1);
                insertBuilder = insertBuilder.Remove(insertBuilder.Length - 1, 1);
                insertBuilder.AppendLine(")");


                //  DataAccess.CreateProcedure(insertBuilder.ToString(), connection);
                //AuditLog.AppendLog(string.Format("Insert stored procedure created for {0} at {1}:{2}", tableName, DateTime.Today.ToLongDateString(), DateTime.Now));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return insertBuilder.ToString();
        }

        private static StringBuilder RemoveLastComma(StringBuilder builder)
        {

            builder = builder.Remove(builder.Length - 1, 1);
            return builder;
        }
        private static StringBuilder RemoveLastComma2(StringBuilder builder)
        {
            int u = builder.ToString().LastIndexOf(",");
            builder = builder.Remove(u, 1);
            return builder;
        }

        #endregion

        #region Private Property
        private static DataAccess DataAccess
        {
            get
            {
                if (_DataAccess == null)
                {
                    _DataAccess = new DataAccess();
                }
                return _DataAccess;
            }
        }
        #endregion
    }
}
