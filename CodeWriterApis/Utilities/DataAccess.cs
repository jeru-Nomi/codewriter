﻿
/******************************************************************
 * Author: Saket Sinha
 * Date Created: 24 June 2007
 * Please do not remove or change the Authors name if using this
 * utility
 * Copyright: Saket Sinha Australia
 *****************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace CodeWriterApi.Utilities
{
    public class DataAccess
    {
        #region Declarations
        #endregion

        #region Methods
        public DataTable GetAllTable(String tableCatalog, SqlConnection connection)
        {
            try
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "TableNameSelect";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TableCatalog", tableCatalog);

                DataTable table = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
            catch (SqlException ex)
            {
                if (ex.Message.StartsWith("Could not find stored procedure"))
                {
                    string message = "Stored procedure to get table name not found.\nDo you want to create it?\nIf you choose 'yes', a Stored procedure will be created.\nTry to get tables again after that.";
                   // DialogResult result = MessageBox.Show(message, "Not found", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                  //  if (result == DialogResult.Yes)
                //    {
                  //      EssentialsStoredProcedures.CreateGetTableNamesProcedure(connection);
                 //   }
                    throw new Exception("Please try again");
                }
                throw new Exception(ex.Message);
            }

        }

       

        public DataTable GetColumnName(String tableName, SqlConnection connection)
        {
            try
            {

                SqlConnection thisconnection = connection;
                thisconnection.Open();
                string st = (@"SELECT 
                            c.name 'COLUMN_NAME',
                            t.Name 'DATA_TYPE',
                            c.max_length 'CHARACTER_MAXIMUM_LENGTH',
                            c.precision ,
                            c.scale ,
                            c.is_nullable,
                            ISNULL(i.is_primary_key, 0) 'Primary Key',
                            case when COLUMNPROPERTY(object_id('@TableName'), c.name, 'IsIdentity') = 1 then 1 else 0 end as AUTO_INCREMENT,
                            ll.table_name, 
							ll.table_type
                            FROM
                        sys.columns c
                        INNER JOIN
                            sys.types t ON c.user_type_id = t.user_type_id
                        LEFT OUTER JOIN
                            sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id
                        LEFT OUTER JOIN
                            sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id
                         left join (SELECT   k.column_name, object_name(ff.referenced_object_id) 'table_name', i.CONSTRAINT_TYPE 'table_type'
									FROM information_schema.TABLE_CONSTRAINTS i
									LEFT JOIN information_schema.KEY_COLUMN_USAGE k
									ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME
									left join sys.foreign_keys ff on ff.name = k.CONSTRAINT_NAME
									where i.TABLE_NAME = '" + tableName + "')ll on ll.COLUMN_NAME =c.name WHERE c.object_id = OBJECT_ID('" + tableName+ "')");
                st = st.Replace("@TableName", tableName);
                DataSet thisdataset = new DataSet();
                //string cmdtext = "select * from receipt_info where receipt_no =='" + i + "'";
                SqlCommand cmd = new SqlCommand(st, thisconnection);
                SqlDataAdapter data_ad = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                data_ad.Fill(dt);


                //SqlCommand command = connection.CreateCommand();
                //command.CommandText = "ColumnNameSelect";
                //command.CommandType = CommandType.StoredProcedure;
                //command.Parameters.AddWithValue("@TableName", tableName);

                //string st = ("select  receipt_no, name, rupees, pay_by, date from receipt_info where receipt_no =" + i);


                //DataTable table = new DataTable();
                //SqlDataAdapter adapter = new SqlDataAdapter();
                //adapter.SelectCommand = command;
               // adapter.Fill(table);
                return dt;
            }
            catch (SqlException ex)
            {
                if (ex.Message.StartsWith("Could not find stored procedure"))
                {
                    string message = "Stored procedure to get column name not found.\nDo you want to create it?\nIf you choose 'yes', a Stored procedure will be created.\nTry to get tables again after that.";
                    //DialogResult result = MessageBox.Show(message, "Not found", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    //if (result == DialogResult.Yes)
                    //{
                    //    EssentialsStoredProcedures.CreateGetColumnsNamesProcedure(connection);
                    //}
                    //throw new Exception("Please try again");
                }
                throw new Exception(ex.Message);
            }
        }

        public string GetSQLQueryText(SqlConnection connection, string proc)
        {
            try
            {

               // SqlCommand cmd = new SqlCommand("sys.sp_helptext", AppCon);
   


                SqlConnection thisconnection = connection;
                thisconnection.Open();
             
               // st = st.Replace("@TableName", tableName);
                DataSet thisdataset = new DataSet();
                //string cmdtext = "select * from receipt_info where receipt_no =='" + i + "'";
                SqlCommand cmd = new SqlCommand("sys.sp_helptext", thisconnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@objname", proc);
                SqlDataAdapter data_ad = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                data_ad.Fill(dt);

                string query = "";
                foreach (DataRow row in dt.Rows)
                {
                    query  += row["Text"].ToString();
                }
                //SqlCommand command = connection.CreateCommand();
                //command.CommandText = "ColumnNameSelect";
                //command.CommandType = CommandType.StoredProcedure;
                //command.Parameters.AddWithValue("@TableName", tableName);

                //string st = ("select  receipt_no, name, rupees, pay_by, date from receipt_info where receipt_no =" + i);


                //DataTable table = new DataTable();
                //SqlDataAdapter adapter = new SqlDataAdapter();
                //adapter.SelectCommand = command;
                // adapter.Fill(table);
                return query;
            }
            catch (SqlException ex)
            {
                if (ex.Message.StartsWith("Could not find stored procedure"))
                {
                    string message = "Stored procedure to get column name not found.\nDo you want to create it?\nIf you choose 'yes', a Stored procedure will be created.\nTry to get tables again after that.";
                    //DialogResult result = MessageBox.Show(message, "Not found", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    //if (result == DialogResult.Yes)
                    //{
                    //    EssentialsStoredProcedures.CreateGetColumnsNamesProcedure(connection);
                    //}
                    //throw new Exception("Please try again");
                }
                throw new Exception(ex.Message);
            }
        }


        public DataTable GetProcSelectParams(SqlConnection connection, string proc)
        {
            try
            {

                // SqlCommand cmd = new SqlCommand("sys.sp_helptext", AppCon);



                SqlConnection thisconnection = connection;
                thisconnection.Open();
                string st = (@"select  
                           'ColumnName' = name,  
                           'ParamName' = null,  
                           'ColumnType' = case when system_type_name like '%nvarchar%' then 'nvarchar' else system_type_name end,
                            'IsNullable' = is_nullable
                            from sys.dm_exec_describe_first_result_set_for_object(OBJECT_ID('" + proc +"'), 0)") ; 
                
                DataSet thisdataset = new DataSet();
                //string cmdtext = "select * from receipt_info where receipt_no =='" + i + "'";
                SqlCommand cmd = new SqlCommand(st, thisconnection);
                SqlDataAdapter data_ad = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                data_ad.Fill(dt);
                return dt;
              
            }
            catch (SqlException ex)
            {
                if (ex.Message.StartsWith("Could not find stored procedure"))
                {
                    string message = "Stored procedure to get column name not found.\nDo you want to create it?\nIf you choose 'yes', a Stored procedure will be created.\nTry to get tables again after that.";
                    //DialogResult result = MessageBox.Show(message, "Not found", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    //if (result == DialogResult.Yes)
                    //{
                    //    EssentialsStoredProcedures.CreateGetColumnsNamesProcedure(connection);
                    //}
                    //throw new Exception("Please try again");
                }
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetProcParams(SqlConnection connection, string proc)
        {
            try
            {

                // SqlCommand cmd = new SqlCommand("sys.sp_helptext", AppCon);



                SqlConnection thisconnection = connection;
                thisconnection.Open();
                string st = (@"select  
                            'ColumnName'  = null,                          
                            'ParamName' = name,  
                           'ColumnType'   = type_name(user_type_id),
                            'IsNullable' = is_nullable
                            from sys.parameters where object_id = object_id('" + proc + "')");

                DataSet thisdataset = new DataSet();
                //string cmdtext = "select * from receipt_info where receipt_no =='" + i + "'";
                SqlCommand cmd = new SqlCommand(st, thisconnection);
                SqlDataAdapter data_ad = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                data_ad.Fill(dt);
                return dt;

            }
            catch (SqlException ex)
            {
                if (ex.Message.StartsWith("Could not find stored procedure"))
                {
                    string message = "Stored procedure to get column name not found.\nDo you want to create it?\nIf you choose 'yes', a Stored procedure will be created.\nTry to get tables again after that.";
                    //DialogResult result = MessageBox.Show(message, "Not found", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    //if (result == DialogResult.Yes)
                    //{
                    //    EssentialsStoredProcedures.CreateGetColumnsNamesProcedure(connection);
                    //}
                    //throw new Exception("Please try again");
                }
                throw new Exception(ex.Message);
            }
        }

        public void CreateProcedure(String query, SqlConnection connection)
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               // AuditLog.AppendLog(ex.Message);
                throw new Exception(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion
    }
}
