﻿using JerSecurityApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace CodeWriterApi.Utilities
{
    public class Converter
    {
        public static string ToCamelCaseFormatColumn(string objName)
        {
            string[] str = objName.Split(new[] { "_" }, StringSplitOptions.None).Select(x => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(x)).ToArray();

            return String.Join("",str);
        }

        public static string ToLowerCaseFormatColumn(string objName)
        {
            if (objName != string.Empty && char.IsUpper(objName[0]))
            {
                objName = char.ToLower(objName[0]) + objName.Substring(1);
            }
            return objName;
        }

        public static string formatProcName(string tableName, CrudAction c)
        {
            string builder = "";
            
                builder += "SP_";

                builder += tableName.Split(new[] { "_" }, StringSplitOptions.None)[0];
               
                    builder += ReturnCorrectEnvFormat(c,tableName);

                builder += tableName.Split(new[] { "_" }, StringSplitOptions.None)[1];


            //else
            //{
            //    builder += "SP_";

            //    builder += tableName.Split(new[] { "_" }, StringSplitOptions.None)[0];
            //    if (c.Action == CrudActionName.SELECT)
            //        builder += "GET";

            //    builder += tableName.Split(new[] { "_" }, StringSplitOptions.None)[1];

            //}

            return builder;
        }

        public static string ReturnCorrectEnvFormat(CrudAction c, string tableName)
        {
            if(c.Action == CrudActionName.SELECT.ToString())
            {
                if (tableName.Contains("Sec_"))
                    return "_Get";
                else
                    return "_GET_";
            }
            if (c.Action == CrudActionName.INSERT.ToString())
            {
                if (tableName.Contains("Sec_"))
                    return "_Create";
                else
                    return "_CREATE_";
            }
            if (c.Action == CrudActionName.UPDATE.ToString())
            {
                if (tableName.Contains("Sec_"))
                    return "_Update";
                else
                    return "_UPDATE_";
            }
            if (c.Action == CrudActionName.DELETE.ToString())
            {
                if (tableName.Contains("Sec_"))
                    return "_Delete";
                else
                    return "_DELETE_";
            }
            return "";   
        }
        public static string ToUpperCase(string objName)
        {
            if (objName != string.Empty && char.IsUpper(objName[0]))
            {
                objName = char.ToUpper(objName[0]) + objName.Substring(1);
            }
            return objName;
        }

        public static string ObjNameFormat(string objName)
        {
            int underLineCount = objName.Count(c => c == '_');
            if (underLineCount > 0)//מתווה
            {
                string[] str = objName.Split(new[] { "_" }, StringSplitOptions.None).Select(x => CultureInfo.CurrentCulture.TextInfo.ToLower(x)).ToArray();
                for(int i=0; i< str.Length; i++ )
                {
                    if(str[i].Length > 1)
                    {
                        str[i] =  char.ToUpper(str[i][0]) + str[i].Substring(1);
                    }
                }
                return String.Join("",str);
            }
            else//jer security
            {
                string[] str = objName.Split(new[] { "_" }, StringSplitOptions.None);
                return String.Join("", str.Skip(1));
            }
        }

        public static string TableFormat(string objName)
        {
            int underLineCount = objName.Count(c => c == '_');
            if (underLineCount > 1)//מתווה
            {
                string[] str = objName.Split(new[] { "_" }, StringSplitOptions.None).Select(x => CultureInfo.CurrentCulture.TextInfo.ToLower(x)).ToArray();
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i].Length > 1)
                    {
                        str[i] = char.ToUpper(str[i][0]) + str[i].Substring(1);
                    }
                }
                return String.Join("", str);
            }
            else
            {
                string[] str = objName.Split(new[] { "_" }, StringSplitOptions.None);
                return String.Join("", str.Skip(1));
            }
        }

        public static string ToCamelCaseFormatTable(string objName)
        {

            int underLineCount = objName.Count(c => c == '_');
            if (underLineCount > 1)//מתווה
            {
                string[] str = objName.Split(new[] { "_" }, StringSplitOptions.None).Select(x => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(x)).ToArray();
                return String.Join("", str.Skip(1));
            }
            else
            {
                string[] str = objName.Split(new[] { "_" }, StringSplitOptions.None);
                return String.Join("", str.Skip(1));
            }
           
        }

        public static SqlDbType StringToSqlType(string s)
        {
            if (s.Contains("varchar"))
                s = "varchar";
            SqlDbType type = (SqlDbType)Enum.Parse(typeof(SqlDbType), s, true);
            return type;
        }
        public static string  GetClrType(SqlDbType sqlType, bool Nullable)
        {
            switch (sqlType)
            {
                case SqlDbType.BigInt:
                    return Nullable ? "long?": "long";

                case SqlDbType.Binary:
                case SqlDbType.Image:
                case SqlDbType.Timestamp:
                case SqlDbType.VarBinary:
                    return "byte[]";

                case SqlDbType.Bit:
                    return Nullable ? "bool?" : "bool";

                case SqlDbType.Char:
                case SqlDbType.NChar:
                case SqlDbType.NText:
                case SqlDbType.NVarChar:
                case SqlDbType.Text:
                case SqlDbType.VarChar:
                case SqlDbType.Xml:
                    return "string";

                case SqlDbType.DateTime:
                case SqlDbType.SmallDateTime:
                case SqlDbType.Date:
                case SqlDbType.Time:
                case SqlDbType.DateTime2:
                    return Nullable ? "DateTime?" : "DateTime";

                case SqlDbType.Decimal:
                case SqlDbType.Money:
                case SqlDbType.SmallMoney:
                    return Nullable ? "decimal?" : "decimal";

                case SqlDbType.Float:
                    return Nullable?  "double?": "double";

                case SqlDbType.Int:
                    return Nullable ? "int?" : "int";

                case SqlDbType.Real:
                    return Nullable ?  "float?" : "float";

                case SqlDbType.UniqueIdentifier:
                    return Nullable ? "Guid?" : "Guid";

                case SqlDbType.SmallInt:
                    return Nullable? "short?" : "short";

                case SqlDbType.TinyInt:
                    return Nullable? "byte?" :"byte";

                case SqlDbType.Variant:
                case SqlDbType.Udt:
                    return "object";

                case SqlDbType.Structured:
                    return "DataTable";

                case SqlDbType.DateTimeOffset:
                    return Nullable? "DateTimeOffset?" : "DateTimeOffset";

                default:
                    throw new ArgumentOutOfRangeException("sqlType");
            }
        }
    }
}