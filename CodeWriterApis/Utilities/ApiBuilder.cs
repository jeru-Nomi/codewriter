﻿/******************************************************************
 * Author: Saket Sinha
 * Date Created: 24 June 2007
 * Please do not remove or change the Authors name if using this
 * utility
 * Copyright: Saket Sinha Australia
 *****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using JerSecurityApi.ViewModels;
using JerSecurityApi.Utilities;
using CodeWriterApi.ViewModels;
using System.Text.RegularExpressions;
using JerSecurityApi.Models;
using ColorCode;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;

namespace CodeWriterApi.Utilities
{
    public class ApiBuilder
    {
        #region declarations
        private static DataAccess _DataAccess;
        #endregion

        #region Constructor
        static ApiBuilder()
        {
        }
        public static string BuildApi(SqlViewModel aa, DataTable columnTable)
        {
            string result = "";
            StringBuilder builder = new StringBuilder();

            string s = GetDBProcedures(aa.Actions, builder);
            SetParams(aa);
            s += GetModel(aa, builder, columnTable);
            s += GetDal(aa, builder, columnTable);
            s += GetController(aa, builder);
            return ArrangeUsingRoslyn(s);
            //string actualHtml = new CodeColorizer().Colorize("public void dd()\r\n{\r\n    return aa\r\n}", Languages.CSharp);
            //return actualHtml;

        }
        #endregion

        #region Private Methods

        public static string ArrangeUsingRoslyn(string csCode)
        {
            var tree = CSharpSyntaxTree.ParseText(csCode);
            var root = tree.GetRoot().NormalizeWhitespace();
            var ret = root.ToFullString();
            return ret;
        }


        public static void SetParams(SqlViewModel sqlViewModel)
        {
            DataAccess access = new DataAccess();
            //////select//////////////////////////////////////////////////////////
            CrudAction selectAction = sqlViewModel.Actions.FindLast(x => x.Action == CrudActionName.SELECT.ToString());
            if (selectAction != null)
            {

                DataTable dtSelectParams = access.GetProcParams(new System.Data.SqlClient.SqlConnection(sqlViewModel.ConnectionString), selectAction.Procedure);
                selectAction.Params = new List<JerSecurityApi.Models.Ccolumn>();
                foreach (DataRow dr in dtSelectParams.Rows)
                {
                    SqlDbType type = (SqlDbType)Enum.Parse(typeof(SqlDbType), dr["ColumnType"].ToString(), true);
                    string cType = Utilities.Converter.GetClrType(type, Convert.ToBoolean(dr["IsNullable"])); //convert sql type to C# type
                    selectAction.Params.Add(new JerSecurityApi.Models.Ccolumn(dr["ColumnName"].ToString(), cType, dr["ParamName"].ToString(), Convert.ToBoolean(dr["IsNullable"].ToString()), false, false, null));

                }
                selectAction.SelectColumns = new List<JerSecurityApi.Models.Ccolumn>();
                DataTable dtSelectData = access.GetProcSelectParams(new System.Data.SqlClient.SqlConnection(sqlViewModel.ConnectionString), selectAction.Procedure);
                foreach (DataRow dr in dtSelectData.Rows)
                {
                    if(dr["ColumnType"].ToString() != "")
                    {
                        SqlDbType type = Utilities.Converter.StringToSqlType(dr["ColumnType"].ToString());
                        string cType = Utilities.Converter.GetClrType(type, Convert.ToBoolean(dr["IsNullable"]));
                        selectAction.SelectColumns.Add(new JerSecurityApi.Models.Ccolumn(dr["ColumnName"].ToString(), cType, dr["ParamName"].ToString(), Convert.ToBoolean(dr["IsNullable"]), false, false, null));


                    }

                }
                //  dtAll.Merge(dtSelectData);
            }


            //////Insert//////////////////////////////////////////////////////////
            CrudAction insertAction = sqlViewModel.Actions.FindLast(x => x.Action == CrudActionName.INSERT.ToString());
            if (insertAction != null)
            {

                DataTable dtInsertParams = access.GetProcParams(new System.Data.SqlClient.SqlConnection(sqlViewModel.ConnectionString), insertAction.Procedure);
                insertAction.Params = new List<JerSecurityApi.Models.Ccolumn>();
                foreach (DataRow dr in dtInsertParams.Rows)
                {

                    SqlDbType type = (SqlDbType)Enum.Parse(typeof(SqlDbType), dr["ColumnType"].ToString(), true);
                    string cType = Utilities.Converter.GetClrType(type, Convert.ToBoolean(dr["IsNullable"])); //convert sql type to C# type
                    insertAction.Params.Add(new JerSecurityApi.Models.Ccolumn(dr["ColumnName"].ToString(), cType, dr["ParamName"].ToString(), Convert.ToBoolean(dr["IsNullable"].ToString()), false, false, null));

                }

                //  dtAll.Merge(dtSelectData);
            }


            //////Update//////////////////////////////////////////////////////////
            CrudAction updateAction = sqlViewModel.Actions.FindLast(x => x.Action == CrudActionName.UPDATE.ToString());
            if (updateAction != null)
            {

                DataTable dtUpdateParams = access.GetProcParams(new System.Data.SqlClient.SqlConnection(sqlViewModel.ConnectionString), updateAction.Procedure);
                updateAction.Params = new List<JerSecurityApi.Models.Ccolumn>();
                foreach (DataRow dr in dtUpdateParams.Rows)
                {
                    SqlDbType type = (SqlDbType)Enum.Parse(typeof(SqlDbType), dr["ColumnType"].ToString(), true);
                    string cType = Utilities.Converter.GetClrType(type, Convert.ToBoolean(dr["IsNullable"]));  //convert sql type to C# type
                    updateAction.Params.Add(new JerSecurityApi.Models.Ccolumn(dr["ColumnName"].ToString(), cType, dr["ParamName"].ToString(), Convert.ToBoolean(dr["IsNullable"].ToString()), false, false, null));

                }

                //  dtAll.Merge(dtSelectData);
            }


            //////Update//////////////////////////////////////////////////////////
            CrudAction deleteAction = sqlViewModel.Actions.FindLast(x => x.Action == CrudActionName.DELETE.ToString());
            if (deleteAction != null)
            {

                DataTable dtDeleteParams = access.GetProcParams(new System.Data.SqlClient.SqlConnection(sqlViewModel.ConnectionString), deleteAction.Procedure);
                deleteAction.Params = new List<JerSecurityApi.Models.Ccolumn>();
                foreach (DataRow dr in dtDeleteParams.Rows)
                {
                    SqlDbType type = (SqlDbType)Enum.Parse(typeof(SqlDbType), dr["ColumnType"].ToString(), true);
                    string cType = Utilities.Converter.GetClrType(type, Convert.ToBoolean(dr["IsNullable"]));  //convert sql type to C# type
                    deleteAction.Params.Add(new JerSecurityApi.Models.Ccolumn(dr["ColumnName"].ToString(), cType, dr["ParamName"].ToString(), Convert.ToBoolean(dr["IsNullable"].ToString()),false, false, null));

                }

                //  dtAll.Merge(dtSelectData);
            }
        }

        public static string GetDBProcedures(List<CrudAction> actions, StringBuilder DBProceduresBuilder)
        {
            StringBuilder bb = new StringBuilder();
            foreach (CrudAction c in actions)
            {
                if (c.Procedure != null)
                {
                    string[] removeExt = c.Procedure.Split(new[] { "_" }, StringSplitOptions.None);
                    string propName = "";
                    for (int i = 2; i < removeExt.Length; i++)
                    {
                        propName += StringExtensions.FirstCharToUpper(removeExt[i]);
                    }
                    c.ApiProcedureName = propName;
                    bb.AppendLine(string.Format(" public static string {0}", propName));
                    bb.AppendLine("{");
                    bb.AppendLine(string.Format(" get {{ return \"{0}\"; }}", c.Procedure));
                    bb.AppendLine("}");
                }

            }

            return bb.ToString();
        }

        public static string GetModel(SqlViewModel view, StringBuilder ModelBuilder, DataTable columnTable)
        {
            ModelBuilder = new StringBuilder();
            List<Ccolumn> Lc = new List<Ccolumn>();
            foreach (CrudAction c in view.Actions)
            {
                if(c.Params != null)
                {
                    foreach (Ccolumn col in c.Params)
                    {
                        if (Lc.FindIndex(x => x.ObjName == col.ObjName) == -1)
                        {
                            Lc.Add(col);
                        }
                    }
                }
               
                if(c.SelectColumns != null)
                {
  foreach (Ccolumn col in c.SelectColumns)
                {
                    if (Lc.FindIndex(x => x.ObjName == col.ObjName) == -1)
                    {
                        Lc.Add(col);
                    }
                }
                }
              

                //Lc.AddRange(c.SelectColumns);
            }


            view.uniqParams = Lc;
            string objName = Converter.TableFormat(view.TableName);
            ModelBuilder.AppendLine(string.Format("public class {0}", objName));
            ModelBuilder.AppendLine("{");
            foreach (Ccolumn c in Lc)
            {
                ModelBuilder.AppendLine(string.Format("[DBName(\"{0}\")]", c.Title == "" ? c.ObjName : c.Title));
                ModelBuilder.AppendLine(string.Format("public {0} {1} {{ get; set; }}", c.ColumnType, c.ObjName));
                ModelBuilder.AppendLine();
            }
            ModelBuilder.AppendLine("}");

            //create filter viewModel
            CrudAction selectAction = view.Actions.FindLast(x => x.Action == CrudActionName.SELECT.ToString());
            if (selectAction != null && selectAction.Params.Count > 2)
            {
                ModelBuilder.AppendLine(string.Format("public class {0}ViewModel", objName));
                ModelBuilder.AppendLine("{");
                foreach (Ccolumn c in selectAction.Params)
                {
                    ModelBuilder.AppendLine(string.Format("public {0} {1} {{ get; set; }}", c.ColumnType, c.ObjName));
                }
                ModelBuilder.AppendLine("}");
            }

            return ModelBuilder.ToString();
        }

        public static string GetController(SqlViewModel view, StringBuilder ControllerBuilder)
        {
            ControllerBuilder = new StringBuilder();
            string objName = Converter.ToCamelCaseFormatTable(view.TableName);
            ControllerBuilder.AppendLine(string.Format("public class {0}Controller: ApiController {{", objName));
            foreach (CrudAction c in view.Actions)
            {
                if (c.Action == CrudActionName.SELECT.ToString())
                {
                    string Params = "";
                    string ParamsSign = "";
                    string sendParams = "";
                    Boolean viewModel = false;
                    if (c.Params.Count > 2)
                    {
                        Params = objName + "ViewModel " + objName;
                        viewModel = true;
                        sendParams = objName;
                        ControllerBuilder.AppendLine(string.Format("[HttpGet]", objName));
                    }
                    else
                    {
                        foreach (Ccolumn col in c.Params)
                        {
                            ParamsSign += "{" + col.ObjName + "},";
                            Params += col.ColumnType + " " + col.ObjName + ",";
                            sendParams += col.ObjName + ",";
                        }
                        Params = Params.TrimEnd(',');
                        ParamsSign = ParamsSign.TrimEnd(',');
                        sendParams = sendParams.TrimEnd(',');
                        ControllerBuilder.AppendLine(string.Format("[HttpPost]", objName));
                    }
                   // ParamsSign ="{"+ParamsSign+"}";
                    ControllerBuilder.AppendLine(string.Format("[Route(\"api/{0}/Get/{1}\")]", objName, ParamsSign));
                    if (!viewModel)
                        ControllerBuilder.AppendLine(string.Format("public IHttpActionResult Get{0}({1})", objName, Params));
                    else
                    {
                        ControllerBuilder.AppendLine(string.Format("public IHttpActionResult Get{1}([FromBody] {0} {1})", Params, objName));
                    }
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("try");
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine(string.Format("return Ok({0}.{1}({2}) );", view.DataLayerName, c.DalFunctionName, sendParams));
                    // ControllerBuilder.Append(string.Format("List<{0}> {4} = {1}.{2}({3});", objName, view.DataLayerName, c.DalFunctionName, sendParams, objName +"List"));
                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("catch (Exception ex)");
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("return new ErrorMessage(ex, HttpStatusCode.InternalServerError, Request);");
                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("}");
                }

                if (c.Action == CrudActionName.INSERT.ToString())
                {

                    ControllerBuilder.AppendLine(string.Format("[HttpPost]", objName));
                    ControllerBuilder.AppendLine(string.Format("[Route(\"api/{0}/Insert\")]", objName));
                    ControllerBuilder.AppendLine(string.Format("public IHttpActionResult Insert{0}([FromBody] {0} {0})", objName));
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("try");
                    ControllerBuilder.AppendLine("{");

                    ControllerBuilder.AppendLine(string.Format("{0}.{1}({2})", view.DataLayerName, c.DalFunctionName, objName));
                    ControllerBuilder.AppendLine(string.Format("return Ok();"));

                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("catch (Exception ex)");
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("return new ErrorMessage(ex, HttpStatusCode.InternalServerError, Request);");
                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("}");

                }

                if (c.Action == CrudActionName.UPDATE.ToString())
                {

                    ControllerBuilder.AppendLine(string.Format("[HttpPut]", objName));
                    ControllerBuilder.AppendLine(string.Format("[Route(\"api/{0}/Update\")]", objName));
                    ControllerBuilder.AppendLine(string.Format("public IHttpActionResult Update{0}([FromBody] {0} {0})", objName));
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("try");
                    ControllerBuilder.AppendLine("{");

                    ControllerBuilder.AppendLine(string.Format("{0}.{1}({2})", view.DataLayerName, c.DalFunctionName, objName));
                    ControllerBuilder.AppendLine(string.Format("return Ok();"));

                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("catch (Exception ex)");
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("return new ErrorMessage(ex, HttpStatusCode.InternalServerError, Request);");
                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("}");

                }

                if (c.Action == CrudActionName.DELETE.ToString())
                {
                    string Params = "";
                    string SignParams = "";
                    string SendParams = "";
                    foreach (Ccolumn col in c.Params)
                    {
                        Params += col.ColumnType + " " + col.ObjName + ",";
                        SendParams += col.ObjName + ",";
                        SignParams += "{" + col.ObjName + "},";
                    }
                    Params = Params.TrimEnd(',');
                    SignParams = SignParams.TrimEnd(',');
                    SendParams = SendParams.TrimEnd(',');


                    ControllerBuilder.AppendLine(string.Format("[HttpDelete]", objName));
                    ControllerBuilder.AppendLine(string.Format("[Route(\"api/{0}/Delete/{1}\")]", objName, SignParams));

                    ControllerBuilder.AppendLine(string.Format("public IHttpActionResult Delete{0}([FromBody] {0} {0})", objName));
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("try");
                    ControllerBuilder.AppendLine("{");

                    ControllerBuilder.AppendLine(string.Format("{0}.{1}({2})", view.DataLayerName, c.DalFunctionName, SendParams));
                    ControllerBuilder.AppendLine(string.Format("return Ok();"));

                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("catch (Exception ex)");
                    ControllerBuilder.AppendLine("{");
                    ControllerBuilder.AppendLine("return new ErrorMessage(ex, HttpStatusCode.InternalServerError, Request);");
                    ControllerBuilder.AppendLine("}");
                    ControllerBuilder.AppendLine("}");

                }
            }
            return ControllerBuilder.ToString();
        }

        public static string GetDal(SqlViewModel view, StringBuilder DalBuilder, DataTable columnTable)
        {
            DalBuilder = new StringBuilder();
            string objName = Converter.ToCamelCaseFormatTable(view.TableName);
            view.DataLayerName = objName + "DataLayer";
            DalBuilder.AppendLine(string.Format("public class {0}DataLayer {{", objName));
            foreach (CrudAction c in view.Actions)
            {

                //Lc.AddRange(c.SelectColumns);
                if (c.Action == CrudActionName.SELECT.ToString())
                {
                    string Params = "";
                    Boolean viewModel = false;
                    if (c.Params.Count > 2)
                    {
                        Params = objName + "ViewModel " + objName;
                        viewModel = true;
                    }
                    else
                    {
                        foreach (Ccolumn col in c.Params)
                        {
                            Params += col.ColumnType + " " + col.ObjName + ",";
                        }
                        Params = Params.TrimEnd(',');
                    }
                    if (c.IsList)
                        DalBuilder.AppendLine(string.Format("public static List<{0}> Get{1}({2}){{", objName, objName, Params));
                    else
                        DalBuilder.AppendLine(string.Format("public static {0} Get{1}({2}){{", objName, objName, Params));

                    c.DalFunctionName = "Get" + objName;

                    DalBuilder.AppendLine(string.Format("SqlParameter[] SqlParameters = new SqlParameter[{0}];", c.Params.Count));
                    for (int i = 0; i < c.Params.Count; i++)
                    {
                        string paramValue = viewModel ? objName + "ViewModel." + c.Params[i].ObjName : c.Params[i].ObjName;
                        DalBuilder.AppendLine(string.Format("SqlParameters[{0}] = new SqlParameter(\"{1}\", {2});", i, c.Params[i].ParamName, paramValue));

                    }

                    if (c.IsList)
                        DalBuilder.AppendLine(string.Format("return DBConverter.ToList<{0}>(DBProcedures.{1}, CommandType.StoredProcedure,SqlParameters);", objName, c.ApiProcedureName, view.ConnectionStringName));
                    else
                        DalBuilder.AppendLine(string.Format("return DBConverter.ToObject<{0}>(MyProps.{2},CommandType.StoredProcedure,DBProcedures.{1},SqlParameters);", objName, c.ApiProcedureName, view.ConnectionStringName));
                    DalBuilder.AppendLine("}");


                }



                if (c.Action == CrudActionName.INSERT.ToString())//// later return new id
                {
                    DalBuilder.AppendLine(string.Format("public static void Insert{0}({0} {0}) {{", objName));

                    c.DalFunctionName = "Insert" + objName;

                    DalBuilder.AppendLine(string.Format("SqlParameter[] SqlParameters = new SqlParameter[{0}];", c.Params.Count));
                    for (int i = 0; i < c.Params.Count; i++)
                    {
                        DalBuilder.AppendLine(string.Format("SqlParameters[{3}] = new SqlParameter(\"{0}\", {1}.{2});", c.Params[i].ParamName, objName, c.Params[i].ObjName, i));

                    }

                    DalBuilder.AppendLine(string.Format("SqlHelper.ExecuteDataset(MyProps.{0}, CommandType.StoredProcedure, \"DBProcedures.{1}\", SqlParameters);", view.ConnectionStringName, c.ApiProcedureName));

                    DalBuilder.AppendLine(String.Format("}}"));
                }

                if (c.Action == CrudActionName.UPDATE.ToString())
                {
                    DalBuilder.AppendLine(string.Format("public static void Update{0}({0} {0}) {{", objName));

                    c.DalFunctionName = "Update" + objName;

                    DalBuilder.AppendLine(string.Format("SqlParameter[] SqlParameters = new SqlParameter[{0}];", c.Params.Count));
                    for (int i = 0; i < c.Params.Count; i++)
                    {
                        DalBuilder.AppendLine(string.Format("SqlParameters[{3}] = new SqlParameter(\"{0}\", {1}.{2});", c.Params[i].ParamName, objName, c.Params[i].ObjName, i));


                    }

                    DalBuilder.AppendLine(string.Format("SqlHelper.ExecuteDataset(MyProps.{0}, CommandType.StoredProcedure, {1}), SqlParameters);", view.ConnectionStringName, c.ApiProcedureName));
                    DalBuilder.AppendLine(String.Format("}}"));
                }

                if (c.Action == CrudActionName.DELETE.ToString())
                {

                    string Params = "";
                    c.DalFunctionName = "Delete" + objName;

                    foreach (Ccolumn col in c.Params)
                    {
                        Params += col.ColumnType + " " + col.ObjName + ",";
                    }
                    Params = Params.TrimEnd(',');


                    DalBuilder.AppendLine(string.Format("public static void Delete{0}({1}) {{", objName, Params));



                    DalBuilder.AppendLine(string.Format("SqlParameter[] SqlParameters = new SqlParameter[{0}];", c.Params.Count));
                    for (int i = 0; i < c.Params.Count; i++)
                    {
                        DalBuilder.AppendLine(string.Format("SqlParameters[{0}] = new SqlParameter(\"{0}\", {1}.{2});", i, c.Params[i].ParamName, objName, c.Params[i].ObjName));

                    }


                    DalBuilder.AppendLine(string.Format("SqlHelper.ExecuteDataset(MyProps.{0}, CommandType.StoredProcedure, {1}), SqlParameters);", view.ConnectionStringName, c.ApiProcedureName));
                }



            }
            DalBuilder.AppendLine(string.Format("}}"));
            return DalBuilder.ToString();
        }


        private static StringBuilder RemoveLastComma(StringBuilder builder)
        {
            builder.Remove(builder.Length - 1, 1);
            return builder;
        }
        #endregion

        #region Private Property
        private static DataAccess DataAccess
        {
            get
            {
                if (_DataAccess == null)
                {
                    _DataAccess = new DataAccess();
                }
                return _DataAccess;
            }
        }
        #endregion
    }
}
